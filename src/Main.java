/**
 * Класс позволяющий разрешить спор: "Что появилось сначала - яйцо или курица?".
 * Один поток выводит сообщение "Курица", другой поток  выводит сообщение "Яйцо".
 * В результате работы этого класса выводится результат спора, в зависимости от того, какой поток сказал последнее слово.
 *
 */
public class Main {
    public static void main(String[] args) {
        Animal chicken = new Animal("Курица");
        Animal egg = new Animal("Яйцо");

        chicken.setOpponent(egg);
        egg.setOpponent(chicken);

        chicken.start();
        egg.start();

        try {
            chicken.join();
            egg.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}