/**
 * Класс для создания потока.
 *
 */
public class Animal extends Thread {
        private Animal other;
        private int counter = 0;
        private String nameOfThread;

        public Animal(String name) {
            this.nameOfThread = name;
        }

        public void setOpponent(Animal opponent) {
            other = opponent;
        }

    /**
     * Метод, который выводит сообщение "яйцо" или "курица", а также определяет победителя.
     * 
     */
    public void run() {
            while((counter++) < 500) {
                System.out.println(nameOfThread);
            }

            if(!other.isAlive()) {
                System.out.printf("Побеждает %s ", nameOfThread);
            }
        }
    }

